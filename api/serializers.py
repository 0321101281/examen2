from rest_framework import serializers
from todo.models import Task

class ListAllTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = "__all__"

class ListAllTaskIdsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = [
            "id",
        ]
        
class ListAllTaskIdUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = [
            "id",
            "user"
        ]

class ListAllTaskIdTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = [
            "id",
            "title"
        ]

class DetailTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = "__all__"

class CreateTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = "__all__"

class UpdateTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = "__all__"