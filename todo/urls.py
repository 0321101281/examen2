from django.urls import path

from todo import views

app_name = 'todo'

urlpatterns = [
    ## TASK URLS ##
    path('list/task/', views.ListTask.as_view(), name="list_task"),
    path('new/task/', views.CreateTask.as_view(), name="create_task"),
    path('detail/task/<int:pk>/', views.DetailTask.as_view(), name="detail_task"),
    path('update/task/<int:pk>/', views.UpdateTask.as_view(), name="update_task"),
    path('delete/task/<int:pk>/', views.DeleteTask.as_view(), name="delete_task"),
]
