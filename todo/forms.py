from django import forms
from .models import Task


class NewTaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = [
            "title",
            "description",
            "user",
            "state",
            "status",
        ]
        widgets = {
            "title": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Write the task title"}),
            "description": forms.Textarea(attrs={"rows": 4, "type": "text", "class": "form-control", "placeholder": "Description of the task"}),
            "user": forms.Select(attrs={"type": "select", "class": "form-control"}),
            "state": forms.Select(attrs={"type": "select", "class": "form-control"}),
            "status": forms.CheckboxInput(attrs={"type": "checkbox"}),
        }

class UpdateTaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = [
            "title",
            "description",
            "user",
            "state",
            "status",
        ]
        widgets = {
            "title": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Write the task title"}),
            "description": forms.Textarea(attrs={"rows": 4, "type": "text", "class": "form-control", "placeholder": "Description of the task"}),
            "user": forms.Select(attrs={"type": "select", "class": "form-control"}),
            "state": forms.Select(attrs={"type": "select", "class": "form-control"}),
            "status": forms.CheckboxInput(attrs={"type": "checkbox"}),
        }
