from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics
from django.views import generic

from .serializers import ListAllTaskSerializer, ListAllTaskIdsSerializer, ListAllTaskIdUserSerializer, ListAllTaskIdTitleSerializer, DetailTaskSerializer, CreateTaskSerializer
from todo.models import Task

## API List
class ApiListView(generic.View):
    template_name = 'api/apis.html'
    def get(self, request):
        return render(request, self.template_name, self.context)

## Create Task
class CreateTaskAPIView(generics.CreateAPIView):
    serializer_class = CreateTaskSerializer

#List Tasks
class ListTaskAPIView(APIView):
    def get(self, request):
        serializer_class = ListAllTaskSerializer
        queryset = Task.objects.all()
        data = serializer_class(queryset, many=True).data
        return Response(data)
    
## Detail Task
class DetailTaskAPIView(APIView):
    def get(self, request, pk):
        queryset = Task.objects.get(pk=pk)
        data = DetailTaskSerializer(queryset, many=False).data
        return Response(data)

## Update Task
class UpdateTaskAPIView(generics.UpdateAPIView):
    serializer_class = CreateTaskSerializer
    queryset = Task.objects.all()

## Delete Task
class DeleteTaskAPIView(generics.DestroyAPIView):
    serializer_class = DetailTaskSerializer
    queryset = Task.objects.all()

## List Tasks (Id)
class ListTaskidsAPIView(APIView):
    def get(self, request):
        serializer_class = ListAllTaskIdsSerializer
        queryset = Task.objects.all()
        data = serializer_class(queryset, many=True).data
        return Response(data)

## List Tasks (Id and Title)
class ListTaskidstitlesAPIView(APIView):
    def get(self, request):
        serializer_class = ListAllTaskIdTitleSerializer
        queryset = Task.objects.all()
        data = serializer_class(queryset, many=True).data
        return Response(data)
    
## List Pending Tasks (Id and Title)
class ListPendingTaskidstitlesAPIView(APIView):
    def get(self, request):
        serializer_class = ListAllTaskIdTitleSerializer
        queryset = Task.objects.filter(state="PENDING")
        data = serializer_class(queryset, many=True).data
        return Response(data)

## List Resolved Tasks (Id and Title)
class ListResolvedTaskidstitlesAPIView(APIView):
    def get(self, request):
        serializer_class = ListAllTaskIdTitleSerializer
        queryset = Task.objects.filter(state="RESOLVED")
        data = serializer_class(queryset, many=True).data
        return Response(data)
    
## List Tasks (Id and User)
class ListTaskidsuserAPIView(APIView):
    def get(self, request):
        serializer_class = ListAllTaskIdUserSerializer
        queryset = Task.objects.all()
        data = serializer_class(queryset, many=True).data
        return Response(data)
    
## List Pending Tasks (Id and User)
class ListPendingTaskidsuserAPIView(APIView):
    def get(self, request):
        serializer_class = ListAllTaskIdUserSerializer
        queryset = Task.objects.filter(state="PENDING")
        data = serializer_class(queryset, many=True).data
        return Response(data)

## List Resolved Tasks (Id and User) 
class ListResolvedTaskidsuserAPIView(APIView):
    def get(self, request):
        serializer_class = ListAllTaskIdUserSerializer
        queryset = Task.objects.filter(state="RESOLVED")
        data = serializer_class(queryset, many=True).data
        return Response(data)