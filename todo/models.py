from django.db import models
from django.contrib.auth.models import User


STATE_OPTIONS = (
    ("PENDING", "PE"),
    ("RESOLVED", "RE")
)


class Task(models.Model):
    title = models.CharField(max_length=50, default="Task title")
    description = models.CharField(max_length=200, default="Task description")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    timestamp =  models.DateTimeField(auto_now_add=True)
    state = models.CharField(max_length=16, choices=STATE_OPTIONS)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.title
    
