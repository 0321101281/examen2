from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy

from .models import Task
from .forms import NewTaskForm, UpdateTaskForm

# # # # CRUD TASK # # # #

## CREATE ## 
class CreateTask(generic.CreateView):
    template_name = 'todo/create_task.html'
    model = Task
    form_class = NewTaskForm
    success_url = reverse_lazy("todo:list_task")

## LIST ##
class ListTask(generic.View):
    template_name = 'todo/list_task.html'
    context = {}

    def get(self, request):
        self.context = {
            'tasks': Task.objects.filter(status=True)
        }
        return render(request, self.template_name, self.context)

## DETAIL ##
class DetailTask(generic.View):
    template_name = 'todo/detail_task.html'
    context = {}

    def get(self, request, pk):
        self.context = {
            'tasks': Task.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)
    
## UPDATE ## 
class UpdateTask(generic.UpdateView):
    template_name = 'todo/update_task.html'
    model =Task
    form_class = UpdateTaskForm
    success_url = reverse_lazy("todo:list_task")

## DELETE ## 
class DeleteTask(generic.DeleteView):
    template_name = 'todo/delete_task.html'
    model = Task
    success_url = reverse_lazy("todo:list_task")