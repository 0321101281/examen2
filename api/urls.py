from django.urls import path

from api import views

app_name = "api"


urlpatterns = [
    path('', views.APIView.as_view(), name="api_list"),

    path('list/task/', views.ListTaskAPIView.as_view(), name="list_task_api"),
    path('list/task-ids/', views.ListTaskidsAPIView.as_view(), name="list_task_ids_api"),

    path('list/task-ids-titles/', views.ListTaskidstitlesAPIView.as_view(), name="list_task_ids_titles_api"),
    path('list/pending/task-ids-titles/', views.ListPendingTaskidstitlesAPIView.as_view(), name="list_pending_task_ids_titles_api"),
    path('list/resolved/task-ids-titles/', views.ListResolvedTaskidstitlesAPIView.as_view(), name="list_resolved_task_ids_titles_api"),

    path('list/task-ids-user/', views.ListTaskidsuserAPIView.as_view(), name="list_task_ids_user_api"),
    path('list/pending/task-ids-user/', views.ListPendingTaskidsuserAPIView.as_view(), name="list_pending_task_ids_user_api"),
    path('list/resolved/task-ids-user/', views.ListResolvedTaskidsuserAPIView.as_view(), name="list_resolved_task_ids_user_api"),

    path('detail/task/<int:pk>/', views.DetailTaskAPIView.as_view(), name="detail_task_api"),
    path('create/task/', views.CreateTaskAPIView.as_view(), name="create_task_api"),
    path('update/task/<int:pk>/', views.UpdateTaskAPIView.as_view(), name="update_task_api"),
    path('delete/task/<int:pk>/', views.DeleteTaskAPIView.as_view(), name="delete_task_api"),
]
